// this program tries to find the next prime fibonacci number.
// i.e. - given a input n, the `nxtPrmFib` function returns a number which is both prime and fibonacci and is greater than the input number.

// the program below is complete and works as detailed in the requirements.
// the task here is to:

// 1. rationalize and understand the code as best you can.
// 2. open a PR to improve the code as you see fit.

// Converting to an Angular app or typescript is not considered a necessity here.
// We're mostly interested in understanding how engineers can critique the code & suggest improvements.

// a) There are many algorithms that are much faster, than just checking divisions for each number till checked number
// b) The namings of functions and variables better write in a way it is easy to understand what it for
// c) There is no need to recalculate Fibonacci for each number, just continue calculating the next value
const isPrime = (number) => {
    for(let i = 2, to = Math.sqrt(number); i <= to; i++)
        if(number % i === 0) return false;
    return number > 1;
}

const nextPrimeFibonacci = (number) => {
    let previous1 = 1;
    let previous2 = 1;
    let fibonacci = null;
    while (fibonacci !== Infinity) {
        fibonacci = previous1 + previous2;
        if(fibonacci > number && isPrime(fibonacci)) {
            return fibonacci;
        }
        previous1 = previous2;
        previous2 = fibonacci;
    }
    return null;
}

nextPrimeFibonacci(20);